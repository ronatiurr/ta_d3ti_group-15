package com.homestay.be.service;

import com.homestay.be.domain.model.Pemesanan;
import com.homestay.be.domain.repository.PemesananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PemesananImpl implements PemesananService {

   @Autowired
   PemesananRepository pemesananRepository;

   @Override
   public Pemesanan findById(String id) {
      return pemesananRepository.findById(Integer.valueOf(id));
   }

   @Override
   public Pemesanan create(Pemesanan request) {
      return pemesananRepository.save(request);
   }

   @Override
   public Pemesanan findByUser(String id) {
      return pemesananRepository.findByUser(Integer.valueOf(id));
   }

   @Override
   public boolean isPemesananExist(String id) {
      return pemesananRepository.findById(Integer.valueOf(id)) != null;
   }
}
