package com.homestay.be.service;

import com.homestay.be.domain.model.Homestay;
import com.homestay.be.domain.repository.HomestayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomestayImpl implements HomestayService {

   @Autowired
   HomestayRepository homestayRepository;

   @Override
   public List<Homestay> findAll() {
      return homestayRepository.findAll();
   }

   @Override
   public Homestay findById(int id) {
      return homestayRepository.findById(id);
   }

   @Override
   public List<Homestay> findByKecamatan(String kecamatan) {
      return homestayRepository.findByKecamatan(Integer.valueOf(kecamatan));
   }

   @Override
   public Homestay create(Homestay homestay) {
      return homestayRepository.save(homestay);
   }

   @Override
   public boolean isHomestayExist(String id) {
      return homestayRepository.findById(Integer.valueOf(id)) != null;
   }
}
