package com.homestay.be.service;

import com.homestay.be.domain.model.Homestay;
import com.homestay.be.domain.model.Kamar;

import java.util.List;

public interface KamarService {
   List<Kamar> findAll();

   Kamar findByNo(int no);

   List<Kamar> findByType(String type);

   Kamar create(Kamar kamar);

   Kamar edit(Kamar kamar);

   void delete(int no);

   Kamar findByTypeAndHomestay(String type, int homestay);

   boolean isKamarExist(String id);
}
