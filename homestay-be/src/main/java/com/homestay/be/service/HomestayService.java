package com.homestay.be.service;

import com.homestay.be.domain.model.Homestay;

import java.util.List;

public interface HomestayService {
   List<Homestay> findAll();

   Homestay findById(int id);

   List<Homestay> findByKecamatan(String kecamatan);

   Homestay create(Homestay homestay);

   boolean isHomestayExist(String id);
}
