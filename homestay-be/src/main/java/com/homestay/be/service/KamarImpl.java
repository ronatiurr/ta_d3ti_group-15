package com.homestay.be.service;

import com.homestay.be.domain.model.Homestay;
import com.homestay.be.domain.model.Kamar;
import com.homestay.be.domain.repository.KamarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KamarImpl implements KamarService {

   @Autowired
   KamarRepository kamarRepository;

//untuk main nampilin daftar kamar
   @Override
   public List<Kamar> findAll() {
      return kamarRepository.findAll();
   }

   //detail
   @Override
   public Kamar findByNo(int no) {
      return kamarRepository.findByNo(no);
   }

   @Override
   public List<Kamar> findByType(String type) {
      return kamarRepository.findByType(type);
   }

   @Override
   public Kamar findByTypeAndHomestay(String type, int homestay) {
      return kamarRepository.findByTypeAndHomestay(type, homestay);
   }



   @Override
   public Kamar create(Kamar kamar) {
      return kamarRepository.save(kamar);
   }

   @Override
   public Kamar edit(Kamar kamar) {
      return kamarRepository.save(kamar);
   }

   @Override
   public void delete(int no) {
       kamarRepository.deleteByNo(no);
   }


   @Override
   public boolean isKamarExist(String id) {
      return kamarRepository.findByNo(Integer.valueOf(id)) != null;
   }
}
