package com.homestay.be.service;

import com.homestay.be.domain.model.User;

import java.util.List;

public interface UserService {
   List<User> findAll();

   User findById(int id);

   User update(int id, User user);

   User create(User user);

   void delete(int id);

   User login(String username, String password, int role);

   boolean isUserExist(String username);
}
