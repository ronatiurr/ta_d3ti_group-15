package com.homestay.be.service;

import com.homestay.be.domain.model.Pendaftaran;
import com.homestay.be.domain.repository.PendaftaranRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PendaftaranImpl implements PendaftaranService {

   @Autowired
   PendaftaranRepository pendaftaranRepository;

   @Override
   public Pendaftaran create(Pendaftaran pendaftaran) {
      return pendaftaranRepository.save(pendaftaran);
   }

   @Override
   public boolean isPendaftaranExist(String id) {
      return pendaftaranRepository.findById(Integer.valueOf(id)) != null;
   }
}
