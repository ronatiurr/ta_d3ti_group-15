package com.homestay.be.service;

import com.homestay.be.domain.model.Pendaftaran;

public interface PendaftaranService {
   Pendaftaran create(Pendaftaran pendaftaran);

   boolean isPendaftaranExist(String id);
}
