package com.homestay.be.service;

import com.homestay.be.domain.model.Pemesanan;
import com.homestay.be.domain.request.pemesanan.CreatePemesananRequest;

public interface PemesananService {
   Pemesanan findById(String id);

   Pemesanan findByUser(String id);

   Pemesanan create(Pemesanan pemesanan);

   boolean isPemesananExist(String id);
}
