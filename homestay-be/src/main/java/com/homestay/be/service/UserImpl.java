package com.homestay.be.service;

import com.homestay.be.domain.model.User;
import com.homestay.be.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImpl implements UserService {

   @Autowired
   UserRepository userRepository;

   @Override
   public List<User> findAll() {
      return userRepository.findAll();
   }

   @Override
   public User findById(int id) {
      return userRepository.findById(id);
   }

   @Override
   public User update(int id, User user) {
      user.getId();
      return userRepository.save(user);
   }

   @Override
   public User create(User user) {
      return userRepository.save(user);
   }

   @Override
   public void delete(int id) {
      userRepository.deleteById(String.valueOf(id));
   }

   @Override
   public User login(String username, String password, int role) {
      return userRepository.findByUsernameAndPasswordAndRole(username, password, role);
   }

   @Override
   public boolean isUserExist(String username) {
      return userRepository.findByUsername(username) != null;
   }
}
