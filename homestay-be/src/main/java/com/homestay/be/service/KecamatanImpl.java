package com.homestay.be.service;

import com.homestay.be.domain.repository.KecamatanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KecamatanImpl implements KecamatanService {

   @Autowired
   KecamatanRepository kecamatanRepository;

   @Override
   public boolean isKecamatanExist(int id) {
      return kecamatanRepository.findById(id) != null;
   }
}
