package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KecamatanRepository extends JpaRepository<Kecamatan, String> {

   Kecamatan findById(int id);
}
