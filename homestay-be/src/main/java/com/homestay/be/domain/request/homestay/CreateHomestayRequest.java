package com.homestay.be.domain.request.homestay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateHomestayRequest {

   @NotNull()
   @NotEmpty()
   private String nama;

   @NotNull()
   private int qty;

   @NotNull()
   private String keterangan;

//   @NotNull()
//   private String gambar;

   @NotNull()
   private String fasilitas;

   @NotNull()
   private String alamat;

   @NotNull()
   private String poi;

   @NotNull()
   private int kecamatan;

   @NotNull()
   private int user;
}
