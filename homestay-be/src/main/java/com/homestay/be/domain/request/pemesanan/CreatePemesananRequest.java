package com.homestay.be.domain.request.pemesanan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatePemesananRequest {

   @NotNull()
   private int user;

   @NotNull()
   private int homestay;

   @NotNull()
   private int kamar;

   @NotNull()
   private int jumlahKamar;

   @NotEmpty
   @NotNull()
   private String in;

   @NotEmpty
   @NotNull()
   private String out;

   @NotNull()
   private int jumlahTamu;

   @NotNull()
   private int total;
}
