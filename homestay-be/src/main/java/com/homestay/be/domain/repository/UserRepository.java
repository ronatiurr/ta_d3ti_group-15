package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

   User findById(int id);

   User findByUsername(String username);

   User findByUsernameAndPasswordAndRole(String username, String password, int role);
}
