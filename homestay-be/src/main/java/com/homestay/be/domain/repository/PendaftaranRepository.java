package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.Pendaftaran;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendaftaranRepository extends JpaRepository<Pendaftaran, String> {

   Pendaftaran findById(int id);
}
