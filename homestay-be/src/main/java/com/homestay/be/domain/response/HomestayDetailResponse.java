package com.homestay.be.domain.response;

import com.homestay.be.domain.model.Homestay;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomestayDetailResponse extends Homestay {
    private int oneBed;
    private int twoBed;
    private int oneBedRoom;
    private int twoBedRoom;
}
