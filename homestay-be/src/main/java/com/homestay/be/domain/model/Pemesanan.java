package com.homestay.be.domain.model;

import com.homestay.be.domain.request.pemesanan.CreatePemesananRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "pemesanan")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class Pemesanan {

   @Id @Column(name = "id_pemesanan",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int id;

   @Column(name = "id_user", length = 11)
   private int user;

   @Column(name = "id_homestay", length = 11)
   private int homestay;

   @Column(name = "no_kamar", length = 11)
   private int kamar;

   @Column(name = "jumlah_kamar", length = 11)
   private int jumlahKamar;

   @Column(name = "check_in", nullable = false, length = 50)
   private String in;

   @Column(name = "check_out", nullable = false, length = 50)
   private String out;

   @Column(name = "jumlah_tamu", length = 11)
   private int jumlahTamu;

   @Column(name = "total_price", length = 11)
   private int total;

   public Pemesanan(CreatePemesananRequest request) {
      this.user = request.getUser();
      this.homestay = request.getHomestay();
      this.kamar = request.getKamar();
      this.jumlahKamar = request.getJumlahKamar();
      this.in = request.getIn();
      this.out = request.getOut();
      this.jumlahTamu = request.getJumlahTamu();
      this.total = request.getTotal();
   }
}
