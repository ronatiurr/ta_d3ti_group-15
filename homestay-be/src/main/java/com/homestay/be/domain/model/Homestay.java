package com.homestay.be.domain.model;

import com.homestay.be.domain.request.homestay.CreateHomestayRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "homestay")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class Homestay {

   @Id @Column(name = "id_homestay",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int id;

   @Column(name = "nama", nullable = false, length = 100)
   private String nama;

   @Column(name = "qty", length = 9)
   private int qty;

   @Column(name = "keterangan", length = 500)
   private String keterangan;

//   @Column(name = "gambar", length = 225)
//   private String gambar;

   @Column(name = "fasilitas", length = 100)
   private String fasilitas;

   @Column(name = "alamat", length = 100)
   private String alamat;

   @Column(name = "poi", length = 500)
   private String poi;

   @Column(name = "id_kec", length = 11)
   private int kecamatan;

   public Homestay(CreateHomestayRequest request) {
      this.nama = request.getNama();
      this.qty = request.getQty();
      this.keterangan = request.getKeterangan();
//      this.gambar = request.getGambar();
      this.fasilitas = request.getFasilitas();
      this.kecamatan = request.getKecamatan();
      this.alamat = request.getAlamat();
      this.poi = request.getPoi();
   }


}
