package com.homestay.be.domain.request.pendaftaran;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatePendaftaranRequest {

   @NotNull()
   private int user;

   @NotNull()
   private int homestay;

   @NotNull()
   private int status;
}
