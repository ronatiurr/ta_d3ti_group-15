package com.homestay.be.domain.request.kamar;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateKamarRequest {

   @NotNull()
   private String harga_kamar;

   @NotNull()
   private String value;

   @NotNull()
   private int id_homestay;

   @NotNull()
   private int jumlah;

}