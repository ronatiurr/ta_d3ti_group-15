package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.Homestay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomestayRepository extends JpaRepository<Homestay, String> {

   Homestay findById(int id);

   List<Homestay> findByKecamatan(int kecamatan);
}
