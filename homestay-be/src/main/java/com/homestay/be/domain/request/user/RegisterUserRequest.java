package com.homestay.be.domain.request.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUserRequest {

   @NotNull()
   @NotEmpty()
   private String username;

   @NotNull()
   @NotEmpty()
   private String email;

   @NotNull()
   @NotEmpty()
   private String password;

   @NotNull()
   @NotEmpty()
   private String nama;

   @NotNull()
   @NotEmpty()
   private String alamat;

   @NotNull()
   @NotEmpty()
   private String nohp;

   @NotNull()
   private int role;
}
