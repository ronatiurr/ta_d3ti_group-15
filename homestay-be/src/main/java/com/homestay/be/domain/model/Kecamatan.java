package com.homestay.be.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "kecamatan")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class Kecamatan {

   @Id @Column(name = "id_kec",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int id;

   @Column(name = "nama_kec", length = 40)
   private String nama;
}
