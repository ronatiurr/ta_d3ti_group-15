package com.homestay.be.domain.model;

import com.homestay.be.domain.request.user.RegisterUserRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "users")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class User {

   @Id @Column(name = "id_user",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int id;

   @Column(name = "nama", length = 100)
   private String nama;

   @Column(name = "alamat", length = 100)
   private String alamat;

   @Column(name = "nohp", length = 15)
   private String nohp;

   @Column(name = "username", nullable = false, length = 100)
   private String username;

   @Column(name = "email", nullable = false, length = 100)
   private String email;

   @Column(name = "password", nullable = false, length = 100)
   private String password;

   @Column(name = "role", nullable = false, length = 3)
   private int role;

   public User(RegisterUserRequest request) {
      this.username = request.getUsername();
      this.email = request.getEmail();
      this.password = request.getPassword();
      this.role = request.getRole();
      this.nama = request.getNama();
      this.nohp = request.getNohp();
      this.alamat = request.getAlamat();
   }
}
