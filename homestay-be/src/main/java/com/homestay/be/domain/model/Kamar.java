package com.homestay.be.domain.model;

import com.homestay.be.domain.request.homestay.CreateHomestayRequest;
import com.homestay.be.domain.request.kamar.CreateKamarRequest;
import com.homestay.be.domain.request.kamar.EditKamarRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "kamar")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class Kamar {

   @Id @Column(name = "no_kamar",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int no;

   @Column(name = "harga_kamar", nullable = false, length = 100)
   private String harga;

   @Column(name = "value", nullable = false, length = 10)
   private String type;

   @Column(name = "id_homestay", length = 11)
   private int homestay;

   @Column(name = "jumlah", length = 11)
   private int jumlah;

   public Kamar(CreateKamarRequest request) {
      this.harga = request.getHarga_kamar();
      this.type = request.getValue();
      this.homestay = request.getId_homestay();
      this.jumlah = request.getJumlah();

   }

   public Kamar(EditKamarRequest request) {
      this.no = request.getNo_kamar();
      this.harga = request.getHarga_kamar();
      this.type = request.getValue();
      this.homestay = request.getId_homestay();
      this.jumlah = request.getJumlah();
   }



}
