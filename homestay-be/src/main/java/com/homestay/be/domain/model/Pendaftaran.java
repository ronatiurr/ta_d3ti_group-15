package com.homestay.be.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "pendaftaran")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
public class Pendaftaran {

   @Id @Column(name = "id_pendaftaran",length = 11) @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private int id;

   @Column(name = "id_user", length = 11)
   private int user;

   @Column(name = "id_homestay", length = 11)
   private int homestay;

   @Column(name = "status", length = 11)
   private int status;

   public Pendaftaran(int user, int homestay, int status) {
      this.user = user;
      this.homestay = homestay;
      this.status = status;
   }
}
