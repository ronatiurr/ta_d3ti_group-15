package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.Pemesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PemesananRepository extends JpaRepository<Pemesanan, String> {

   Pemesanan findById(int id);

   Pemesanan findByUser(int id);
}
