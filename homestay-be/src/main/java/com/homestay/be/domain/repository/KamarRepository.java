package com.homestay.be.domain.repository;

import com.homestay.be.domain.model.Kamar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KamarRepository extends JpaRepository<Kamar, String> {

   Kamar findByNo(int no);

   List<Kamar> findByHomestay(int homestay);

   Kamar findByTypeAndHomestay(String type, int homestay);

   Kamar findByHarga(String harga);

   void deleteByNo(int no);

   List<Kamar> findByType(String type);
}
