package com.homestay.be.controller;

import com.homestay.be.domain.model.Homestay;
import com.homestay.be.domain.model.Kamar;
import com.homestay.be.domain.model.Pendaftaran;
import com.homestay.be.domain.request.homestay.CreateHomestayRequest;
import com.homestay.be.domain.response.BaseResponse;
import com.homestay.be.domain.response.HomestayDetailResponse;
import com.homestay.be.service.HomestayService;
import com.homestay.be.service.KamarService;
import com.homestay.be.service.KecamatanService;
import com.homestay.be.service.PendaftaranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.homestay.be.preference.MessageStatus.NOT_FOUND;
import static com.homestay.be.preference.MessageStatus.SUCCESS;

@RestController
@RequestMapping(value = "homestay")
public class HomestayController {

    @Autowired
    HomestayService homestayService;

    @Autowired
    KamarService kamarService;

    @Autowired
    PendaftaranService pendaftaranService;

    @Autowired
    KecamatanService kecamatanService;

    @Autowired
    MessageSource messageSource;

    @GetMapping("/search/kecamatan/{id}")
    ResponseEntity<BaseResponse> searchByKecamatan (@PathVariable(value = "id") String id) {
        BaseResponse response = new BaseResponse();

        if (kecamatanService.isKecamatanExist(Integer.valueOf(id))){
            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(homestayService.findByKecamatan(id));
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/detail/{id}")
    ResponseEntity<BaseResponse> getDetail (@PathVariable(value = "id") String id) {
        BaseResponse response = new BaseResponse();

        if (homestayService.isHomestayExist(id)){
            Homestay homestay = homestayService.findById(Integer.valueOf(id));
            HomestayDetailResponse data = new HomestayDetailResponse();
            Kamar oneBed = kamarService.findByTypeAndHomestay("1", homestay.getId());
            Kamar twoBed = kamarService.findByTypeAndHomestay("2", homestay.getId());

            data.setNama(homestay.getNama());
            data.setAlamat(homestay.getAlamat());
            data.setFasilitas(homestay.getFasilitas());
            data.setId(homestay.getId());
//            data.setGambar(homestay.getGambar());
            data.setKecamatan(homestay.getKecamatan());
            data.setKeterangan(homestay.getKeterangan());
            data.setPoi(homestay.getPoi());
            data.setOneBed(oneBed.getJumlah());
            data.setTwoBed(twoBed.getJumlah());
            data.setOneBedRoom(oneBed.getNo());
            data.setTwoBedRoom(twoBed.getNo());

            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(data);
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping("/create")
    ResponseEntity<BaseResponse> create (@RequestBody @Validated CreateHomestayRequest request) {
        BaseResponse response = new BaseResponse();
        Homestay data = new Homestay(request);

        if (kecamatanService.isKecamatanExist(request.getKecamatan())){
            homestayService.create(data);
            pendaftaranService.create(new Pendaftaran(request.getUser(), data.getId(), 0));

            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(homestayService.create(data));
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
