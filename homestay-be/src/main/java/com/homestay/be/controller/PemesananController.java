package com.homestay.be.controller;

import com.homestay.be.domain.model.Pemesanan;
import com.homestay.be.domain.request.pemesanan.CreatePemesananRequest;
import com.homestay.be.domain.response.BaseResponse;
import com.homestay.be.service.PemesananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.homestay.be.preference.MessageStatus.NOT_FOUND;
import static com.homestay.be.preference.MessageStatus.SUCCESS;

@RestController
@RequestMapping(value = "pemesanan")
public class PemesananController {

    @Autowired
    PemesananService pemesananService;

    @PostMapping("/create")
    ResponseEntity<BaseResponse> create (@RequestBody @Validated CreatePemesananRequest request) {
        BaseResponse response = new BaseResponse();
        System.out.println(request);
        response.setCode(200);
        response.setMessage(SUCCESS);
        response.setData(pemesananService.create(new Pemesanan(request)));

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/detail/{id}")
    ResponseEntity<BaseResponse> detail(@PathVariable(value = "id") String id) {
        BaseResponse response = new BaseResponse();

        response.setCode(200);
        response.setMessage(SUCCESS);
        response.setData(pemesananService.findByUser(id));

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

}
