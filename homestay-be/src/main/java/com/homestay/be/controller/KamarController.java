package com.homestay.be.controller;

import com.homestay.be.domain.model.Homestay;
import com.homestay.be.domain.model.Kamar;
import com.homestay.be.domain.model.Pendaftaran;
import com.homestay.be.domain.repository.KamarRepository;
import com.homestay.be.domain.request.homestay.CreateHomestayRequest;
import com.homestay.be.domain.request.kamar.CreateKamarRequest;
import com.homestay.be.domain.request.kamar.EditKamarRequest;
import com.homestay.be.domain.response.BaseResponse;
import com.homestay.be.service.KamarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.homestay.be.preference.MessageStatus.NOT_FOUND;
import static com.homestay.be.preference.MessageStatus.SUCCESS;

@RestController
@RequestMapping(value = "kamar")
public class KamarController {

    @Autowired
    KamarService kamarService;

    @GetMapping("/search/kamar")
    ResponseEntity<BaseResponse> findAll() {
        BaseResponse response = new BaseResponse();

//        if (kamarService.isKamarExist(){
            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(kamarService.findAll());
//        } else {
//            response.setCode(400);
//            response.setMessage(NOT_FOUND);
//        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/detail/{id}")
    ResponseEntity<BaseResponse> getDetail(@PathVariable(value = "id") String id) {
        BaseResponse response = new BaseResponse();

        if (kamarService.isKamarExist(id)) {
            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(kamarService.findByNo(Integer.valueOf(id)));
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/delete/{id}")
    ResponseEntity<BaseResponse> delete(@PathVariable(value = "id") String id) {
        BaseResponse response = new BaseResponse();

        if (kamarService.isKamarExist(id)){
            kamarService.delete(Integer.parseInt(id));

            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData("");
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }


        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping("/create")
    ResponseEntity<BaseResponse> create (@RequestBody @Validated CreateKamarRequest request) {
        BaseResponse response = new BaseResponse();
        Kamar data = new Kamar(request);

        kamarService.create(data);

        response.setCode(200);
        response.setMessage(SUCCESS);
        response.setData(kamarService.create(data));

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping("/edit")
    ResponseEntity<BaseResponse> edit (@RequestBody @Validated EditKamarRequest request) {
        BaseResponse response = new BaseResponse();
        Kamar data = new Kamar(request);

        if (kamarService.isKamarExist(String.valueOf(request.getNo_kamar()))){
            kamarService.edit(data);

            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(kamarService.edit(data));
        } else {
            response.setCode(400);
            response.setMessage(NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }


}
