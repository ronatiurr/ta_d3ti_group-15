package com.homestay.be.controller;

import com.homestay.be.domain.model.User;
import com.homestay.be.domain.request.user.LoginUserRequest;
import com.homestay.be.domain.request.user.RegisterUserRequest;
import com.homestay.be.domain.response.BaseResponse;
import com.homestay.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.homestay.be.preference.MessageStatus.*;

@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/register")
    ResponseEntity<BaseResponse> register (@RequestBody @Validated RegisterUserRequest request) {
        BaseResponse response = new BaseResponse();
        if (userService.isUserExist(request.getUsername())){
            response.setCode(400);
            response.setMessage(USER_EXIST);
        } else {
            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(userService.create(new User(request)));
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping("/pengunjung/login")
    ResponseEntity<BaseResponse> loginPengunjung (@RequestBody @Validated LoginUserRequest request) {
        BaseResponse response = new BaseResponse();
        User data = userService.login(request.getUsername(), request.getPassword(), 2);
        return getLoginResponse(response, data);
    }

    @PostMapping("/pemilik/login")
    ResponseEntity<BaseResponse> loginPemilik (@RequestBody @Validated LoginUserRequest request) {
        BaseResponse response = new BaseResponse();
        User data = userService.login(request.getUsername(), request.getPassword(), 3);
        return getLoginResponse(response, data);
    }

    private ResponseEntity<BaseResponse> getLoginResponse(BaseResponse response, User data) {
        if (data != null) {
            response.setCode(200);
            response.setMessage(SUCCESS);
            response.setData(data);
        } else {
            response.setCode(400);
            response.setMessage(WRONG_AUTH);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }


}
