package com.homestay.be.preference;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface MessageStatus {
    String NOT_FOUND = "Data tidak ditemukan";
    String USER_EXIST = "Username telah terdaftar";
    String WRONG_AUTH = "Username/Password salah";
    String SUCCESS = "Success";
    String ERROR = "Ada kesalahan pada sistem kami";
}

