package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class LoginRequest {
    @JsonField(name = ["username"])
    var inputEmail = ""
    @JsonField(name = ["password"])
    var inputPassword = ""

    constructor(username: String, password: String){
        this.inputEmail = username
        this.inputPassword = password
    }

    constructor()
}
