package com.app.network.networks;

import com.app.network.model.request.*
import com.app.network.model.response.BaseResponse
import com.app.network.model.response.GetHomestayResponse
import com.app.network.model.response.SearchHomestayResponse
import com.app.network.model.response.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface Service {
    @POST("/user/pemilik/login")
    fun login(@Body request: LoginRequest): Single<LoginResponse>

    @POST("/user/register")
    fun register(@Body request: RegisterRequest): Single<BaseResponse>

    @POST("/homestay/create")
    fun createHomestay(@Body request: CreateHomestayRequest): Single<BaseResponse>

    @GET("/homestay/search/kecamatan/{id}")
    fun getHomestayByKecamatan(@Path("id") id: String): Single<SearchHomestayResponse>

    @GET("/homestay/detail/{id}")
    fun getHomestay(@Path("id") id: String): Single<GetHomestayResponse>

//    @GET("/homestay/search/kecamatan")
//    fun getHomestay(): Single<SearchHomestayResponse>

    @POST("/file/upload/{file}")
    @Multipart
    fun upload(@Part file: MultipartBody.Part): Single<BaseResponse>

    @GET("/kamar/search/kamar")
    fun listDetail(): Single<SearchKamarResponse>

    @GET("/kamar/detail/{id}")
    fun getKamar(@Path("id") id: String): Single<GetKamarResponse>

//    @GET("/kamar/detail/{id}")
//    fun getKamar(@Path("id") id: String): Single<GetKamarResponse>

    @POST("/pemesanan/create")
    fun pesanKamar(@Body request: PemesananKamarRequest): Single<BaseResponse>

    @GET("/pemesanan/detail/{id}")
    fun getPemesanan(@Path("id") id: String): Single<GetPemesananResponse>

    @POST("/kamar/delete/{id}")
    fun deleteKamar(@Body request: String): Single<BaseResponse>

    @POST("/kamar/edit")
    fun editKamar(@Body request: EditKamarRequest): Single<BaseResponse>

    @POST("/kamar/create")
    fun createKamar(@Body request: CreateKamarRequest): Single<BaseResponse>
}
