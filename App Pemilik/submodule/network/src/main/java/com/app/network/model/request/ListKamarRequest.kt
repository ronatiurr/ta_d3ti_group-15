package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject


@JsonObject
class ListKamarRequest {
    @JsonField(name = ["nomor"])
    var no_kamar = ""
    @JsonField(name = ["value"])
    var value = ""
    @JsonField(name = ["hargakamar"])
    var hargakamar = ""
    @JsonField(name = ["fasilitas"])
    var fasilitas = ""

    fun isValid(): Boolean{
        return (no_kamar.isNotEmpty()
                && value.isNotEmpty())
    }
}