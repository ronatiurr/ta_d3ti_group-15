package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class PemesananKamarRequest {
    @JsonField(name = ["id"])
    var id = ""
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["jumlah"])
    var jumlah = ""

    //ini untuk ngecek user udah ngisi inputan tadi atau belum
    fun isValid(): Boolean {
        return (id.isNotEmpty())
                &&(nama.isNotEmpty()
                && (jumlah.isNotEmpty()))
    }}