package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class PemesananRequest {
    @JsonField(name = ["harga"])
    var harga_kamar = ""
    @JsonField(name = ["value"])
    var value = ""

    fun isValid(): Boolean{
        return (harga_kamar.isNotEmpty()
                && value.isNotEmpty())
    }
}