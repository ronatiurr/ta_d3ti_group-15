package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class EditKamarRequest {
//    @JsonField(name = ["nomor"])
//    var no_kamar = ""
    @JsonField(name = ["jumlahbed"])
    var jumlahbed = ""
    @JsonField(name = ["hargakamar"])
    var hargakamar = ""
//    @JsonField(name = ["fasilitas"])
//    var fasilitas = ""

    fun isValid(): Boolean{
        return (jumlahbed.isNotEmpty()
                && hargakamar.isNotEmpty())
    }
}