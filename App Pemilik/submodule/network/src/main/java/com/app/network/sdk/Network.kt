package com.app.network.sdk;
import com.app.network.model.request.*
import com.app.network.model.response.*
import com.app.network.response.ResponseCallback;

import io.reactivex.disposables.CompositeDisposable;

interface Network {

    fun login(request: LoginRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<LoginResponse>)
    fun register(request: RegisterRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun createHomestay(request: CreateHomestayRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun getHomestayByKecamatan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<SearchHomestayResponse>)
    fun getListKamar( compositeDisposable: CompositeDisposable, callback: ResponseCallback<SearchKamarResponse>)
    fun getHomestay(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetHomestayResponse>)
    fun getKamar(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetKamarResponse>)
    fun pesanKamar(request: PemesananKamarRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun createKamar(request: CreateKamarRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun getPemesanan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetPemesananResponse>)
    fun editKamar(request: EditKamarRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun deleteKamar(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
}