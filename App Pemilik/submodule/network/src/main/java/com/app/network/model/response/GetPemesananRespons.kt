package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class GetPemesananRespons : BaseResponse() {
    @JsonField(name = ["data"])
    var data : DataPemesanan? = DataPemesanan()
}

@JsonObject
class DataPemesanan {
    @JsonField(name = ["id"])
    var id = ""
    @JsonField(name = ["user"])
    var user = ""
    @JsonField(name = ["homestay"])
    var homestay = ""
    @JsonField(name = ["kamar"])
    var kamar = 0
    @JsonField(name = ["jumlahKamar"])
    var jumlahKamar = 0
    @JsonField(name = ["in"])
    var checkIn = ""
    @JsonField(name = ["out"])
    var checkOut = ""
    @JsonField(name = ["jumlahTamu"])
    var jumlahTamu = 0
    @JsonField(name = ["total"])
    var total = 0
}