package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable


@JsonObject
class GetPemesananResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data = ArrayList<PesananDetail>()
}

@JsonObject
class PesananDetail : Serializable {
    @JsonField(name = ["hargakamar"])
    var hargakamar = ""
    @JsonField(name = ["jumlah"])
    var jumlah = ""
    @JsonField(name = ["nomor kamar"])
    var no_kamar = ""
    @JsonField(name = ["fasilitas"])
    var fasilitas = ""
    @JsonField(name = ["gambar"])
    var gambar = ""
}