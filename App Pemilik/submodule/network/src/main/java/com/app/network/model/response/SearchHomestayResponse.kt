package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable

@JsonObject
class SearchHomestayResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data = ArrayList<HomestayDetail>()
}

@JsonObject
class HomestayDetail : Serializable {
    @JsonField(name = ["id"])
    var id = ""
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["qty"])
    var qty = ""
    @JsonField(name = ["keterangan"])
    var keterangan = ""
//    @JsonField(name = ["gambar"])
//    var gambar = ""
    @JsonField(name = ["alamat"])
    var alamat = ""
    @JsonField(name = ["poi"])
    var poi = ""
    @JsonField(name = ["kecamatan"])
    var kecamatan = ""
    @JsonField(name = ["oneBed"])
    var oneBed = 0
    @JsonField(name = ["twoBed"])
    var twoBed = 0
    @JsonField(name = ["oneBedRoom"])
    var oneBedRoom = 0
    @JsonField(name = ["twoBedRoom"])
    var twoBedRoom = 0
}