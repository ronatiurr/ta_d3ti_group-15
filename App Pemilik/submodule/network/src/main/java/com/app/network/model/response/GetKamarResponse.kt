package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable


@JsonObject
class GetKamarResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data =GetKamarDetail()
}

@JsonObject
class GetKamarDetail : Serializable {
    @JsonField(name = ["hargakamar"])
    var hargakamar = ""
    @JsonField(name = ["jumlahbed"])
    var jumlahbed = ""
//    @JsonField(name = ["nomor kamar"])
//    var no_kamar = ""
//    @JsonField(name = ["fasilitas"])
//    var fasilitas = ""
//    @JsonField(name = ["gambar"])
//    var gambar = ""
}