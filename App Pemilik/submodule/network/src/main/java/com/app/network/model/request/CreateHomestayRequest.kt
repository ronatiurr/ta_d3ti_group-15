package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class CreateHomestayRequest {
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["qty"])
    var qty = ""
    @JsonField(name = ["keterangan"])
    var keterangan = ""
//    @JsonField(name = ["gambar"])
//    var gambar = ""
    @JsonField(name = ["fasilitas"])
    var fasilitas = ""
    @JsonField(name = ["alamat"])
    var alamat = ""
    @JsonField(name = ["poi"])
    var poi = ""
    @JsonField(name = ["kecamatan"])
    var kecamatan = 0
    @JsonField(name = ["user"])
    var user = 0

    constructor()

    fun isValid(): Boolean {
        return (nama.isNotEmpty()
                && (kecamatan != 0))
}}