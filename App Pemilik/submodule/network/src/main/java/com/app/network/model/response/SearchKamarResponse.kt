package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable


@JsonObject
class SearchKamarResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data = ArrayList<ListDetail>()
}

@JsonObject
class ListDetail : Serializable {
    @JsonField(name = ["no"])
    var no = ""
    @JsonField(name = ["harga"])
    var harga = ""
    @JsonField(name = ["type"])
    var type = ""
    @JsonField(name = ["homestay"])
    var homestay = ""
    @JsonField(name = ["jumlah"])
    var jumlah = ""
//    @JsonField(name = ["alamat"])
//    var alamat = ""
//    @JsonField(name = ["poi"])
//    var poi = ""
//    @JsonField(name = ["kecamatan"])
//    var kecamatan = ""
//    @JsonField(name = ["oneBed"])
//    var oneBed = 0
//    @JsonField(name = ["twoBed"])
//    var twoBed = 0
//    @JsonField(name = ["oneBedRoom"])
//    var oneBedRoom = 0
//    @JsonField(name = ["twoBedRoom"])
//    var twoBedRoom = 0
}