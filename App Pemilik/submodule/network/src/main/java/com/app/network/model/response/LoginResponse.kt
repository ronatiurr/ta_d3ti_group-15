package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class LoginResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data : DataLogin? = DataLogin()
}

@JsonObject
class DataLogin {
    @JsonField(name = ["id"])
    var id = ""
    @JsonField(name = ["username"])
    var username = ""
    @JsonField(name = ["email"])
    var email = ""
    @JsonField(name = ["password"])
    var password = ""
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["alamat"])
    var alamat = ""
    @JsonField(name = ["nohp"])
    var nohp = ""
}