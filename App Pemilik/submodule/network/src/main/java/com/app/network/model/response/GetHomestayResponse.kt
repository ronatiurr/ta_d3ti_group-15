package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable

@JsonObject
class GetHomestayResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data = HomestayDetail()
}
