package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class CreateKamarRequest {
    @JsonField(name = ["hargakamar"])
    var hargakamar = ""
    @JsonField(name = ["jumlahbed"])
    var jumlahbed = ""
//    @JsonField(name = ["nomor kamar"])
//    var no_kamar = ""
//    @JsonField(name = ["fasilitas"])
//    var fasilitas = ""
//    @JsonField(name = ["gambar"])
//    var gambar = ""
//    @JsonField(name = ["poi"])
//    var poi = ""
//    @JsonField(name = ["kecamatan"])
//    var kecamatan = 0
//    @JsonField(name = ["user"])
//    var user = 0

    constructor()

    fun isValid(): Boolean {
        return (hargakamar.isNotEmpty()
                && (jumlahbed.isNotBlank()))
    }
}