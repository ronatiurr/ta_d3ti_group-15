package com.app.network.networks;


import com.app.common.preference.LocalPreferences;
import com.app.network.model.request.*
import com.app.network.model.response.*
import com.app.network.response.ResponseCallback
import com.app.network.rx.BaseSchedulerProvider;
import io.reactivex.disposables.Disposable

class NetworkService(private val service: Service, private val localPreferences: LocalPreferences,
                      private val schedulers: BaseSchedulerProvider) {

    fun login(request: LoginRequest, callback: ResponseCallback<LoginResponse>): Disposable {
        return service.login(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun register(request: RegisterRequest, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.register(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun createHomestay(request: CreateHomestayRequest, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.createHomestay(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun getHomestayByKecamatan(request: String, callback: ResponseCallback<SearchHomestayResponse>): Disposable {
        return service.getHomestayByKecamatan(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }
//listkamar
    fun getListKamar(callback: ResponseCallback<SearchKamarResponse>): Disposable {
        return service.listDetail()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun getHomestay(request: String, callback: ResponseCallback<GetHomestayResponse>): Disposable {
        return service.getHomestay(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun getKamar(request: String, callback: ResponseCallback<GetKamarResponse>): Disposable {
        return service.getKamar(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun pesanKamar(request: PemesananKamarRequest, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.pesanKamar(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun getPemesanan(request: String, callback: ResponseCallback<GetPemesananResponse>): Disposable {
        return service.getPemesanan(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun editKamar(request: EditKamarRequest, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.editKamar(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun createKamar(request: CreateKamarRequest, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.createKamar(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }

    fun deleteKamar(request: String, callback: ResponseCallback<BaseResponse>): Disposable {
        return service.deleteKamar(request)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({ response -> callback.onSuccess(response) }, { throwable -> callback.onError(throwable) })
    }
}
