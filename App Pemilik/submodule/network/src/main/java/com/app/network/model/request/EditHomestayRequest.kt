package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class EditHomestayRequest {
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["keterangan"])
    var keterangan = ""

    fun isValid(): Boolean{
        return (nama.isNotEmpty()
                && keterangan.isNotEmpty())
    }
}