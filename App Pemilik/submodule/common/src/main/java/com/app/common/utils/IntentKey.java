package com.app.common.utils;

import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface IntentKey {
    @Nullable
    String KAMARID = "kamarId";
    @Nullable
    String HOMESTAYID = "homestayId";
    @Nullable
    String FASILITAS = "fasilitas";

}

