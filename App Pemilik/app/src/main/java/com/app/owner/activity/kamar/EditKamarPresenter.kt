package com.app.owner.activity.kamar

import com.app.network.model.request.EditKamarRequest
import com.app.network.model.response.BaseResponse
import com.app.network.model.response.GetKamarResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class EditKamarPresenter (private val view: EditKamarContract.View,
                          private val networkSDK: NetworkSDK) : EditKamarContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getData(request: String) {
        view.loading()

        networkSDK.getKamar(request, compositeDisposable, object : ResponseCallback<GetKamarResponse> {
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: GetKamarResponse) {
                if (response.isSuccess()) view.successEdit()
                else view.failedEdit(response.message)

                view.dismissLoading()
            }

        })
    }

    override fun editData(request: EditKamarRequest) {
        view.loading()

        networkSDK.editKamar(request, compositeDisposable, object : ResponseCallback<BaseResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successEdit()
                else view.failedEdit(response.message)

                view.dismissLoading()
            }

        })
    }
}