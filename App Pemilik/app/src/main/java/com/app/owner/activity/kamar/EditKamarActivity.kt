package com.app.owner.activity.kamar

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.common.utils.IntentKey
import com.app.network.model.request.EditKamarRequest
import com.app.network.model.response.GetKamarDetail
//import com.app.network.model.response.KamarDetail
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_creat_kamar.*
//import kotlinx.android.synthetic.main.activity_detail_kamar.*
import kotlinx.android.synthetic.main.activity_edit_kamar.*
//import kotlinx.android.synthetic.main.activity_edit_kamar.edit
//import kotlinx.android.synthetic.main.activity_edit_kamar.keterangan
import javax.inject.Inject

class EditKamarActivity : BaseActivity(), EditKamarContract.View {

    @Inject
    lateinit var presenter: EditKamarPresenter
    private var request = EditKamarRequest()
    private var no_kamar = ""

    companion object {
        private val TAG = "KamarEditActivity"

        @JvmStatic fun newIntent(context: Context, id: String): Intent {
            val i = Intent(context, EditKamarActivity::class.java)
            i.putExtra(IntentKey.KAMARID, id)

            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_kamar)

        getData()
        initListener()
    }

    private fun initListener(){
        hargakamar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.hargakamar = hargakamar.text.toString()
            }
        })

        jlhbed.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.jumlahbed = jlhbed.text.toString()
            }
        })

        button_save.setOnClickListener { presenter.editData(request) }
    }

    private fun getData(){
        intent.extras?.let { intent -> (intent.getSerializable(IntentKey.KAMARID) as String).let {
            presenter.getData(it)
        } }
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successGetData(response: GetKamarDetail) {
        response.let {
            jlhbed.setText(it.jumlahbed)
            hargakamar.setText(it.hargakamar)
        }
    }

    override fun failedGetData(message: String) {
        toast(message)
    }

    override fun successEdit() {
        finish()
    }

    override fun failedEdit(message: String) {
        toast(message)
    }
}