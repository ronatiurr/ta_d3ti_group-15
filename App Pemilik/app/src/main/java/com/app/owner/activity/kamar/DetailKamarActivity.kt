package com.app.owner.activity.kamar

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.common.utils.IntentKey
import com.app.network.model.response.GetKamarDetail
//import com.app.network.model.response.KamarDetail
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail_kamar.*
import javax.inject.Inject

 abstract class DetailKamarActivity : BaseActivity(), DetailKamarContract.View {

    @Inject
    lateinit var presenter: DetailKamarPresenter
    private var id = ""

    companion object {
        private val TAG = "LoginActivity"

        @JvmStatic fun newIntent(context: Context, id: String): Intent {
            val i = Intent(context, DetailKamarActivity::class.java)
            i.putExtra(IntentKey.KAMARID, id)

            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kamar)

        getData()
        initListener()
    }

    private fun initListener(){
//
        button_delete.setOnClickListener {
                       presenter.deleteData(id) }

        button_edit.setOnClickListener {
            startActivity(EditKamarActivity.newIntent(this, id))
        }
    }

    private fun getData(){
        intent.extras?.let { intent -> (intent.getSerializable(IntentKey.KAMARID) as String).let {
            presenter.getData(it)
        } }
    }

//    private fun toPemesanan(room: String){
//        startActivity(PemesananKamarActivity.newIntent(this, room, fasilitas))
//        finish()
//    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successGetData(response: GetKamarDetail) {
        response.let {
//             no_kamar.text = it.no_kamar
            hargakamar.setText(it.hargakamar)
            jlhbed.setText(it.jumlahbed)

        }
    }

    override fun failedGetData(message: String) {
        toast(message)
    }

    override fun successDelete() {
        finish()
    }

    override fun failedDelete(message: String) {
        toast(message)
    }


}