package com.app.owner.activity.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.app.network.model.response.GetKamarDetail
import com.app.network.model.response.GetKamarResponse
import com.app.network.model.response.SearchKamarResponse
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import com.app.owner.activity.kamar.DetailKamarActivity
import com.app.owner.adapter.KamarAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail_kamar.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity(), MainContract.View, KamarAdapter.KamarAdapterListener{


    @Inject lateinit var presenter: MainPresenter
    private var request = -1
    private var adapter = KamarAdapter(this)


    companion object {
        private val TAG = "MainActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initAdapter()
        presenter.getListKamar()
    }

    override fun onClickItemAdapter(id: String) {
        startActivity(DetailKamarActivity.newIntent(this, id))
    }

    private fun initAdapter() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcList.layoutManager = layoutManager
        rcList.adapter = adapter
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

//    override fun successSearch(response: GetKamarResponse) {
//    }

    override fun successSearch(response: SearchKamarResponse) {
        response.let {
//            no_kamar.text = it.no_kamar
//            hargakamar.setText(it.hargakamar)
//            fasilitas.setText(it.fasilitas)

            adapter.initData(response.data)
        }
    }

    override fun failedSearch(message: String) {
        toast(message)
    }


//    btnTambahKamar.setOnClickListener{ presenter.main(LoginRequest(inputEmail.text.toString(), inputPassword.text.toString())) }
//}


}