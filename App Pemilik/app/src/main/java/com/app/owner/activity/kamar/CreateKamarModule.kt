package com.app.owner.activity.kamar

import com.app.common.preference.LocalPreferences
import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class CreateKamarModule {
    @Provides
    internal fun provideCreateKamarView(activity: CreateKamarActivity): CreateKamarContract.View {
        return activity
    }

    @Provides
    internal fun provideCreateKamarPresenter(view: CreateKamarContract.View, networkSDK: NetworkSDK, localPreferences: LocalPreferences): CreateKamarPresenter {
        return CreateKamarPresenter(view, networkSDK, localPreferences)
    }
}