package com.app.owner.deps

////import com.app.owner.activity.SplashActivity
//import com.app.owner.activity.homestay.DetailHomestayActivity
//import com.app.owner.activity.homestay.DetailHomestayModule
import com.app.owner.activity.SplashActivity
import com.app.owner.activity.homestay.CreateHomestayActivity
import com.app.owner.activity.homestay.CreateHomestayModule
//import com.app.owner.activity.homestay.pembayaran.PembayaranActivity
//import com.app.owner.activity.homestay.pembayaran.PembayaranModule
//import com.app.owner.activity.homestay.pemesanan.PemesananHomestayActivity
//import com.app.owner.activity.homestay.pemesanan.PemesananHomestayActivitynanHomestayModule
//import com.app.owner.activity.homestay.pemesanan.detail.DetailPesananActivity
//import com.app.owner.activity.homestay.pemesanan.detail.DetailPesananModule
import com.app.owner.activity.kamar.*
import com.app.owner.activity.login.LoginActivity
import com.app.owner.activity.login.LoginModule
import com.app.owner.activity.main.MainActivity
import com.app.owner.activity.main.MainModule
import com.app.owner.activity.register.RegisterActivity
import com.app.owner.activity.register.RegisterModule
//import com.app.owner.activity.register.RegisterActivity
//import com.app.owner.activity.register.RegisterModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector
    internal abstract fun splashActivity(): SplashActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LoginModule::class])
    internal abstract fun loginActivity(): LoginActivity

//    @ActivityScoped
//    @ContributesAndroidInjector(modules = arrayOf(LoginModule::class))
//    internal abstract fun loginActivity(): LoginActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(MainModule::class))
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [CreateHomestayModule::class])
    internal abstract fun createHomestayActivity(): CreateHomestayActivity

//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [EditHomestayModule::class])
//    internal abstract fun editHomestayActivity(): EditHomestayActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [DetailHomestayModule::class])
//    internal abstract fun detailHomestayActivity(): DetailHomestayActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [PemesananHomestayModule::class])
//    internal abstract fun pemesananHomestayActivity(): PemesananHomestayActivity

//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [DetailPesananModule::class])
//    internal abstract fun detailPesananActivity(): DetailPesananActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [PembayaranModule::class])
//    internal abstract fun pembayaranActivity(): PembayaranActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [RegisterModule::class])
    internal abstract fun registerActivity(): RegisterActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [CreateKamarModule::class])
    internal abstract fun createKamarActivity(): CreateKamarActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [EditKamarModule::class])
    internal abstract fun editKamarActivity(): EditKamarActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [DetailKamarModule::class])
    internal abstract fun detailKamarActivity(): DetailKamarActivity

}
