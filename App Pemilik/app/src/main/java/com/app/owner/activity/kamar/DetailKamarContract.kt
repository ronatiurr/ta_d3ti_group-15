package com.app.owner.activity.kamar

import com.app.network.model.response.GetKamarDetail

class DetailKamarContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successGetData(response: GetKamarDetail)
        fun failedGetData(message: String)
        fun successDelete()
        fun failedDelete(message: String)
    }

    interface Presenter{
        fun getData(request: String)
        fun deleteData(request: String)
    }

}