package com.app.owner.activity.kamar

import android.net.Uri
import com.app.network.model.request.CreateKamarRequest

class CreateKamarContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successCreate()
        fun failedCreate(message: String)
//        fun successUpload(url: String)
//        fun failedUpload(message: String)
    }

    interface Presenter {
        fun create(request: CreateKamarRequest)
//        fun upload(request: Uri)
    }
}