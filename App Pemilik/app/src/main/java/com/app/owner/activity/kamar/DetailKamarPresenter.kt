package com.app.owner.activity.kamar

import com.app.network.model.response.BaseResponse
import com.app.network.model.response.GetHomestayResponse
import com.app.network.model.response.GetKamarResponse
//import com.app.network.model.response.SearchKamarResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
//import com.app.owner.activity.homestay.DetailHomestayContract
import io.reactivex.disposables.CompositeDisposable

class DetailKamarPresenter (private val view: DetailKamarContract.View,
                            private val networkSDK: NetworkSDK) : DetailKamarContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getData(request: String) {
        view.loading()

        networkSDK.getKamar(request, compositeDisposable, object : ResponseCallback<GetKamarResponse> {
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: GetKamarResponse) {
                if (response.isSuccess()) view.successGetData(response.data)
                else view.failedGetData(response.message)

                view.dismissLoading()
            }

        })
    }

    override fun deleteData(request: String) {
        view.loading()

        networkSDK.deleteKamar(request, compositeDisposable, object : ResponseCallback<BaseResponse> {
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successDelete()
                else view.failedDelete(response.message)

                view.dismissLoading()
            }

        })
    }

}