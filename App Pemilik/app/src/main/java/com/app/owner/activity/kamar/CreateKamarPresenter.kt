package com.app.owner.activity.kamar

import android.net.Uri
import com.app.common.preference.LocalPreferences
import com.app.network.model.request.CreateKamarRequest
import com.app.network.model.response.BaseResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.disposables.CompositeDisposable
import java.util.*

class CreateKamarPresenter (private val view: CreateKamarContract.View,
                            private val networkSDK: NetworkSDK,
                            private val localPreferences: LocalPreferences) : CreateKamarContract.Presenter  {

    private val compositeDisposable = CompositeDisposable()

    override fun create(request: CreateKamarRequest) {
        view.loading()

        networkSDK.createKamar(request, compositeDisposable, object : ResponseCallback<BaseResponse> {
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successCreate()
                else view.failedCreate(response.message)

                view.dismissLoading()
            }

        })
}
//    override fun upload(request: Uri) {
//        view.loading()
//
//        val storageRef = FirebaseStorage.getInstance().getReference("images/" + UUID.randomUUID())
//        storageRef.putFile(request)
//                .addOnFailureListener{
//                    view.dismissLoading()
//                    view.failedNetwork()
//                }
//                .addOnSuccessListener {
//                    storageRef.downloadUrl
//                            .addOnSuccessListener { view.successUpload(it.toString()) }
//                            .addOnFailureListener { view.failedUpload(it.toString()) }
//
//                    view.dismissLoading()
//                }
//    }
}