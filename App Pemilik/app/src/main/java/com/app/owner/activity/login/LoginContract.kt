package com.app.owner.activity.login

import com.app.network.model.request.LoginRequest

class LoginContract {

    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successLogin()
        fun failedLogin(message: String)
    }

    interface Presenter {
        fun login(request: LoginRequest)

    }
}
