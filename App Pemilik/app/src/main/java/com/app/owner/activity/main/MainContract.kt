package com.app.owner.activity.main

import com.app.network.model.response.GetKamarDetail
import com.app.network.model.response.GetKamarResponse
import com.app.network.model.response.SearchKamarResponse

class MainContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successSearch(response: SearchKamarResponse)
        fun failedSearch(message: String)
    }

    interface Presenter {
        fun getListKamar()
    }
}
