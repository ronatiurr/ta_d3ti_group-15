package com.app.owner.activity.kamar

import com.app.common.preference.LocalPreferences
import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class DetailKamarModule {
    @Provides
    internal fun provideDetailKamarView(activity: DetailKamarActivity): DetailKamarContract.View {
        return activity
    }

    @Provides
    internal fun provideDetailKamarPresenter(view: DetailKamarContract.View, networkSDK: NetworkSDK): DetailKamarPresenter {
        return DetailKamarPresenter(view, networkSDK)
    }
}