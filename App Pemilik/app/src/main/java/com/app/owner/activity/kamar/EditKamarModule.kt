package com.app.owner.activity.kamar

import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class EditKamarModule {
    @Provides
    internal fun provideEditKamarView(activity: EditKamarActivity): EditKamarContract.View {
        return activity
    }

    @Provides
    internal fun provideEditKamarPresenter(view: EditKamarContract.View, networkSDK: NetworkSDK): EditKamarPresenter {
        return EditKamarPresenter(view, networkSDK)
    }
}