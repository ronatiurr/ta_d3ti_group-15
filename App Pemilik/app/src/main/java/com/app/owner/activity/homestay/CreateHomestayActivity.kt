package com.app.owner.activity.homestay

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import com.app.network.model.request.CreateHomestayRequest
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import com.app.owner.activity.kamar.CreateKamarActivity
import com.app.owner.activity.login.LoginActivity
import com.app.owner.activity.main.MainActivity
import com.google.firebase.storage.FirebaseStorage
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_create_homestay.*
import kotlinx.android.synthetic.main.activity_create_homestay.alamat
import kotlinx.android.synthetic.main.activity_create_homestay.nama
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*
import javax.inject.Inject

class CreateHomestayActivity : BaseActivity(), CreateHomestayContract.View {

    @Inject
    lateinit var presenter: CreateHomestayPresenter
//    lateinit var presenter
    private var request = CreateHomestayRequest()

    companion object {
        private val TAG = "CreateHomestayActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, CreateHomestayActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_homestay)

        initListener()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
//            presenter.upload(data?.data!!)
        }
    }

    private fun initListener(){
        nama.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.nama = nama.text.toString()
                isValidBtn()
            }
        })

        qty.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.qty = qty.text.toString()
                isValidBtn()
            }
        })

        keterangan.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.keterangan = keterangan.text.toString()
                isValidBtn()
            }
        })

        kecamatan.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (kecamatan.text.isNotEmpty()){
                    request.kecamatan = kecamatan.text.toString().toInt()
                }
                isValidBtn()
            }
        })

        fasilitas.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.fasilitas = fasilitas.text.toString()
                isValidBtn()
            }

        })

        alamat.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.alamat = alamat.text.toString()
                isValidBtn()
            }

        })

        poi.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.poi = poi.text.toString()
                isValidBtn()
            }

        })

//        gambar.setOnClickListener { openGallery() }
        createBtn.setOnClickListener { presenter.create(request) }
        daftarkamar.setOnClickListener { startActivity(MainActivity.newIntent(this)) }

    }

    private fun openGallery(){
        val iImg = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(iImg, 0)
    }

    private fun isValidBtn(){
        createBtn.isEnabled = request.isValid()
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successCreate() {

            startActivity(CreateKamarActivity.newIntent(this))
            finish()

    }

    override fun failedCreate(message: String) {
        toast(message)
    }

//    override fun successUpload(url: String) {
//        request.gambar = url
//    }
//
//    override fun failedUpload(message: String) {
//        toast(message)
//    }


}