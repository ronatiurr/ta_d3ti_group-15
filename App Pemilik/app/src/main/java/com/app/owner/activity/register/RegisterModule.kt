package com.app.owner.activity.register

import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class RegisterModule {
    @Provides
    internal fun provideRegisterView(activity: RegisterActivity): RegisterContract.View {
        return activity
    }

    @Provides
    internal fun provideRegisterPresenter(view: RegisterContract.View, networkSDK: NetworkSDK): RegisterPresenter {
        return RegisterPresenter(view, networkSDK)
    }
}