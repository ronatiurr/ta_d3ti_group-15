package com.app.owner.activity.main

import com.app.network.model.request.ListKamarRequest
import com.app.network.model.response.GetKamarDetail
import com.app.network.model.response.GetKamarResponse
import com.app.network.model.response.SearchKamarResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class MainPresenter(private val view: MainContract.View,
                    private val networkSDK: NetworkSDK) : MainContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    override fun getListKamar() {
        view.loading()

        networkSDK.getListKamar(compositeDisposable, object : ResponseCallback<SearchKamarResponse> { //variabel
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: SearchKamarResponse) {
                if (response.isSuccess()) view.successSearch(response)
                else view.failedSearch(response.message)

                view.dismissLoading()
            }

        })
    }


}
