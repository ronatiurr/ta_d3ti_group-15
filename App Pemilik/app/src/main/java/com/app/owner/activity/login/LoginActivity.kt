package com.app.owner.activity.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.network.model.request.LoginRequest
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import com.app.owner.activity.homestay.CreateHomestayActivity
import com.app.owner.activity.kamar.CreateKamarActivity
import com.app.owner.activity.main.MainActivity
import com.app.owner.activity.register.RegisterActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : BaseActivity(), LoginContract.View {

    @Inject lateinit var presenter: LoginPresenter

    companion object {
        private val TAG = "LoginActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initListener()
    }

    private fun initListener(){
        inputEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isValidBtn()
            }
        })

        inputPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isValidBtn()
            }
        })

//        loginBtn.setOnClickListener{ startActivity(RegisterActivity.newIntent(this)) }
        btnLogin.setOnClickListener{ presenter.login(LoginRequest(inputEmail.text.toString(), inputPassword.text.toString())) }
        btnRegister.setOnClickListener{ startActivity(RegisterActivity.newIntent(this)) }
    }

    private fun isValidBtn(){
        if (inputEmail.text.isNotEmpty() && inputPassword.text.isNotEmpty()){
            btnLogin.isEnabled = true
            btnLogin.setBackgroundResource(R.drawable.bg_btn_enable)
        } else {
            btnLogin.isEnabled = false
            btnLogin.setBackgroundResource(R.drawable.bg_btn_disable)
        }
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successLogin() {
        startActivity(CreateHomestayActivity.newIntent(this))
        finish()
    }

    override fun failedLogin(message: String) {
        toast(message)
    }
}

