package com.app.owner.activity.kamar

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import com.app.network.model.request.CreateKamarRequest
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import com.app.owner.activity.main.MainActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_creat_kamar.*
//import kotlinx.android.synthetic.main.activity_creat_kamar.no_kamar
import kotlinx.android.synthetic.main.activity_daftar_kamar.*
import javax.inject.Inject

class CreateKamarActivity : BaseActivity(), CreateKamarContract.View {

    @Inject
    lateinit var presenter: CreateKamarPresenter
    private var request = CreateKamarRequest()

    companion object {
        private val TAG = "CreateKamarActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, CreateKamarActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creat_kamar)

        initListener()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
//            presenter.upload(data?.data!!)
        }
    }

    private fun initListener() {
        hargakamarr.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.hargakamar = hargakamarr.text.toString()
                isValidBtn()
            }
        })

        jumlahbed.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                request.jumlahbed = jumlahbed.text.toString()
                isValidBtn()
            }

        })

    }

//    private fun openGallery(){
//        val iImg = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        startActivityForResult(iImg, 0)
//    }

    private fun isValidBtn(){
        btnTambah.isEnabled = request.isValid()
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successCreate() {
//        toast("success")
        startActivity(MainActivity.newIntent(this))
        finish()
    }

    override fun failedCreate(message: String) {
        toast(message)
    }

//    override fun successUpload(url: String) {
//        request.gambar = url
//    }
//
//    override fun failedUpload(message: String) {
//        toast(message)
//    }

}