package com.app.owner.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.app.owner.R
import com.app.owner.activity.kamar.CreateKamarActivity
import com.app.owner.activity.login.LoginActivity
import com.app.owner.activity.main.MainActivity

class SplashActivity : AppCompatActivity() {

    // This is the loading time of the splash screen
    private val SPLASH_TIME_OUT:Long = 3000 // 1 sec
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity


            startActivity(LoginActivity.newIntent(this))
//           startActivity(Intent(this, MainActivity::class.java))
           // startActivity(LoginActivity.newIntent(this))


            // close this activity
            finish()
        }, SPLASH_TIME_OUT)
    }
}
