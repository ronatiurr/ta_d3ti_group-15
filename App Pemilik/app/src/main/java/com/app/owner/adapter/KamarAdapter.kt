package com.app.owner.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.network.model.response.GetKamarDetail
import com.app.network.model.response.ListDetail
import com.app.owner.R
import com.app.uicustom.adapter.BaseHolder
import kotlinx.android.synthetic.main.activity_daftar_kamar.view.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.lang.Exception

class KamarAdapter (private val listener: KamarAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = ArrayList<ListDetail>()
    inner class ViewHolder(parent: ViewGroup) : BaseHolder(R.layout.activity_daftar_kamar, parent){
        fun bind(data: ListDetail){
            itemView.no_kamar.setText(data.no)
            itemView.hargakamar.setText(data.harga)
            itemView.type.setText(data.type)
         //   itemView.homestay.setText(data.homestay)
//            itemView.jumlah.setText(data.jumlah)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = ViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[holder.adapterPosition]
        val viewHolder = holder as ViewHolder

        viewHolder.bind(item)
//        viewHolder.itemView.no_kamar=item.no_kamar
//        viewHolder.itemView.harga=item.hargakamar
//        viewHolder.itemView.fasilitas=item.fasilitas
        viewHolder.itemView.lihat.setOnClickListener{
            listener.onClickItemAdapter(item.no)
        }
    }

    fun initData(items: ArrayList<ListDetail>){
        this.items = items
        notifyDataSetChanged()
    }

    interface KamarAdapterListener{
        fun onClickItemAdapter(id: String)
    }
}