package com.app.owner.activity.kamar

import com.app.network.model.request.EditKamarRequest
import com.app.network.model.response.GetKamarDetail
//import com.app.network.model.response.KamarDetail

class EditKamarContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successEdit()
        fun failedEdit(message: String)
        fun successGetData(response: GetKamarDetail)
        fun failedGetData(message: String)
    }

    interface Presenter{
        fun getData(request: String)
        fun editData(request: EditKamarRequest)
    }
}