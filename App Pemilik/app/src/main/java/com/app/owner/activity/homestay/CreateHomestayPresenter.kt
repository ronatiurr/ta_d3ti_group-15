package com.app.owner.activity.homestay

import android.net.Uri
import com.app.common.preference.LocalPreferences
import com.app.network.model.request.CreateHomestayRequest
import com.app.network.model.response.BaseResponse
import com.app.network.model.response.SearchKamarResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import com.app.owner.activity.main.MainContract
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.disposables.CompositeDisposable
import java.util.*
class CreateHomestayPresenter (private val view: CreateHomestayContract.View,
                               private val networkSDK: NetworkSDK,
                               private val localPreferences: LocalPreferences) : CreateHomestayContract.Presenter{

    private val compositeDisposable = CompositeDisposable()

    override fun create(request: CreateHomestayRequest) {
        view.loading()
        request.user = localPreferences.id

        networkSDK.createHomestay(request, compositeDisposable, object : ResponseCallback<BaseResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successCreate()
                else view.failedCreate(response.message)

                view.dismissLoading()
            }

        })
    }




//    override fun upload(request: Uri) {
//        view.loading()
//
//        val storageRef = FirebaseStorage.getInstance().getReference("images/" + UUID.randomUUID())
//        storageRef.putFile(request)
//                .addOnFailureListener{
//                    view.dismissLoading()
//                    view.failedNetwork()
//                }
////                .addOnSuccessListener {
////                    storageRef.downloadUrl
////                            .addOnSuccessListener { view.successUpload(it.toString()) }
////                            .addOnFailureListener { view.failedUpload(it.toString()) }
////
////                    view.dismissLoading()
////                }
//    }

}