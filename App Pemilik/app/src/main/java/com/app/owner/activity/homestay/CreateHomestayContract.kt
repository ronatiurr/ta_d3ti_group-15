package com.app.owner.activity.homestay

import android.net.Uri
import com.app.network.model.request.CreateHomestayRequest
import com.app.network.model.response.SearchKamarResponse

class CreateHomestayContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successCreate()
        fun failedCreate(message: String)


//        fun successUpload(url: String)
//        fun failedUpload(message: String)
    }

    interface Presenter {
        fun create(request: CreateHomestayRequest)

//        fun upload(request: Uri)
    }
}