package com.app.owner.activity.homestay

import dagger.Module
import dagger.Provides
import com.app.common.preference.LocalPreferences
import com.app.network.sdk.NetworkSDK

@Module
class CreateHomestayModule {
    @Provides
    internal fun provideCreateHomestayView(activity: CreateHomestayActivity): CreateHomestayContract.View {
        return activity
    }

    @Provides
    internal fun provideCreateHomestayPresenter(view: CreateHomestayContract.View, networkSDK: NetworkSDK, localPreferences: LocalPreferences): CreateHomestayPresenter {
        return CreateHomestayPresenter(view, networkSDK, localPreferences)
    }
}
