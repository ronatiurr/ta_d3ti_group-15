package com.app.owner.activity.register

import com.app.network.model.request.RegisterRequest


class RegisterContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successRegister()
        fun failedRegister(message: String)
    }

    interface Presenter {
        fun register(request: RegisterRequest)
    }
}