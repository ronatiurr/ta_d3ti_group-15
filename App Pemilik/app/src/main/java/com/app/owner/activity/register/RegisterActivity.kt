package com.app.owner.activity.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.text.Editable
import android.text.TextWatcher
import com.app.network.model.request.RegisterRequest
import com.app.owner.R
import com.app.owner.activity.base.BaseActivity
import com.app.owner.activity.login.LoginActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

class RegisterActivity : BaseActivity(), RegisterContract.View {

    @Inject lateinit var presenter: RegisterPresenter
    private var request = RegisterRequest()

    companion object {
        private val TAG = "RegisterActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initListener()
    }

    private fun initListener(){
        nama.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.nama = nama.text.toString()
                isValidBtn()
            }
        })

        alamat.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.alamat = alamat.text.toString()
                isValidBtn()
            }
        })

        hp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.nohp = hp.text.toString()
                isValidBtn()
            }
        })

        username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.username = username.text.toString()
                isValidBtn()
            }
        })

        email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.email = email.text.toString()
                isValidBtn()
            }

        })

        password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                request.password = password.text.toString()
                isValidBtn()
            }
        })

        login.setOnClickListener { startActivity(LoginActivity.newIntent(this)) }
        registerBtn.setOnClickListener{presenter.register(request)}
    }

    private fun isValidBtn(){
        registerBtn.isEnabled = request.isValid()
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successRegister() {
        startActivity(LoginActivity.newIntent(this))
        finish()
    }

    override fun failedRegister(message: String) {
        toast(message)
    }
}