package com.app.network.networks

import com.app.network.model.request.LoginRequest
import com.app.network.model.request.PemesananRequest
import com.app.network.model.request.RegisterRequest
import com.app.network.model.response.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface Service {
    @POST("/user/pengunjung/login")
    fun login(@Body request: LoginRequest): Single<LoginResponse>

    @POST("/user/register")
    fun register(@Body request: RegisterRequest): Single<BaseResponse>

    @GET("/homestay/search/kecamatan/{id}")
    fun getHomestayByKecamatan(@Path("id") id: String): Single<SearchHomestayResponse>

    @GET("/homestay/detail/{id}")
    fun getHomestay(@Path("id") id: String): Single<GetHomestayResponse>

    @GET("/kamar/detail/{id}")
    fun getKamar(@Path("id") id: String): Single<GetKamarResponse>

    @POST("/pemesanan/create")
    fun pesanKamar(@Body request: PemesananRequest): Single<BaseResponse>

    @GET("/pemesanan/detail/{id}")
    fun getPemesanan(@Path("id") id: String): Single<GetPemesananResponse>
}
