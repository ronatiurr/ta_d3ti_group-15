package com.app.network.sdk

import com.app.network.model.request.LoginRequest
import com.app.network.model.request.PemesananRequest
import com.app.network.model.request.RegisterRequest
import com.app.network.model.response.*
import com.app.network.networks.NetworkService
import com.app.network.response.ResponseCallback
import io.reactivex.disposables.CompositeDisposable

class NetworkSDK(private val service: NetworkService) : Network {
    override fun login(request: LoginRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<LoginResponse>) {
        val disposable = service.login(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun register(request: RegisterRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>) {
        val disposable = service.register(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun getHomestayByKecamatan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<SearchHomestayResponse>) {
        val disposable = service.getHomestayByKecamatan(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun getHomestay(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetHomestayResponse>) {
        val disposable = service.getHomestay(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun getKamar(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetKamarResponse>) {
        val disposable = service.getKamar(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun pesanKamar(request: PemesananRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>) {
        val disposable = service.pesanKamar(request, callback)
        compositeDisposable.add(disposable)
    }

    override fun getPemesanan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetPemesananResponse>) {
        val disposable = service.getPemesanan(request, callback)
        compositeDisposable.add(disposable)
    }
}
