package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class PemesananRequest {
    @JsonField(name = ["user"])
    var user = ""
    @JsonField(name = ["homestay"])
    var homestay = ""
    @JsonField(name = ["kamar"])
    var kamar = ""
    @JsonField(name = ["jumlahKamar"])
    var jumlahKamar = 0
    @JsonField(name = ["in"])
    var checkIn = ""
    @JsonField(name = ["out"])
    var checkOut = ""
    @JsonField(name = ["jumlahTamu"])
    var jumlahTamu = 0
    @JsonField(name = ["total"])
    var total = 0

    fun isValid(): Boolean{
        return ((jumlahKamar != 0)
                && checkIn.isNotEmpty()
                && checkOut.isNotEmpty()
                && (jumlahTamu != 0)
                && (total != 0))
    }
}
