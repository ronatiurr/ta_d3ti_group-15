package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class LoginRequest {
    @JsonField(name = ["username"])
    var username = ""
    @JsonField(name = ["password"])
    var password = ""

    constructor(username: String, password: String){
        this.username = username
        this.password = password
    }

    constructor()
}
