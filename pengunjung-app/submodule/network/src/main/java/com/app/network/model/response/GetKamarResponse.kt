package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable

@JsonObject
class GetKamarResponse : BaseResponse() {
    @JsonField(name = ["data"])
    var data : DataKamar? = DataKamar()
}

@JsonObject
class DataKamar {
    @JsonField(name = ["no"])
    var id = ""
    @JsonField(name = ["harga"])
    var harga = ""
    @JsonField(name = ["type"])
    var type = ""
    @JsonField(name = ["homestay"])
    var homestay = ""
    @JsonField(name = ["jumlah"])
    var jumlah = 0
}
