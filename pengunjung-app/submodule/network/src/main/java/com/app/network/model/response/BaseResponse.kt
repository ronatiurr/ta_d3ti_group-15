package com.app.network.model.response

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.io.Serializable

@JsonObject
open class BaseResponse : Serializable {

    @JsonField(name = ["message"])
    var message = ""
    @JsonField(name = ["code"])
    var code = 0

    constructor()

    fun isSuccess(): Boolean {
        return code == 200
    }
}
