package com.app.network.sdk

import com.app.network.model.request.LoginRequest
import com.app.network.model.request.PemesananRequest
import com.app.network.model.request.RegisterRequest
import com.app.network.model.response.*
import com.app.network.response.ResponseCallback

import io.reactivex.disposables.CompositeDisposable

interface Network {
    fun login(request: LoginRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<LoginResponse>)
    fun register(request: RegisterRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun getHomestayByKecamatan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<SearchHomestayResponse>)
    fun getHomestay(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetHomestayResponse>)
    fun getKamar(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetKamarResponse>)
    fun pesanKamar(request: PemesananRequest, compositeDisposable: CompositeDisposable, callback: ResponseCallback<BaseResponse>)
    fun getPemesanan(request: String, compositeDisposable: CompositeDisposable, callback: ResponseCallback<GetPemesananResponse>)
}
