package com.app.network.model.request

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class RegisterRequest {
    @JsonField(name = ["username"])
    var username = ""
    @JsonField(name = ["email"])
    var email = ""
    @JsonField(name = ["password"])
    var password = ""
    @JsonField(name = ["nama"])
    var nama = ""
    @JsonField(name = ["alamat"])
    var alamat = ""
    @JsonField(name = ["nohp"])
    var nohp = ""
    @JsonField(name = ["role"])
    var role = 0

    constructor() {
        this.role = 2
    }

    fun isValid(): Boolean{
        return (username.isNotEmpty()
                && email.isNotEmpty()
                && password.isNotEmpty()
                && nama.isNotEmpty()
                && alamat.isNotEmpty()
                && nohp.isNotEmpty())
    }
}
