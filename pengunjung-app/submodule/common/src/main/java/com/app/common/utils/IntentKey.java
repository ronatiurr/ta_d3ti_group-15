package com.app.common.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface IntentKey {
    String HOMESTAYID = "homestayId";
    String KAMARID = "kamarId";
    String FASILITAS = "fasilitas";
}

