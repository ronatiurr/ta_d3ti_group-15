package com.app.common.utils;

import android.text.format.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun getDayOfTheWeek(dateStr: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    try {
        val date = dateFormat.parse(dateStr)
        return DateFormat.format("EEEE", date) as String
    } catch (e: Exception) {
        return "-"
    }

}

fun stringToDate(date: String): Date {
    val formatter = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH)

    return formatter.parse(date)
}

fun convertToRp(value: Long?): String {
    if (value == null) {
        return "Rp - "
    }
    return try {
        val decimalFormat = NumberFormat.getNumberInstance(Locale.ITALY) as DecimalFormat
        "Rp ${decimalFormat.format(value)}"
    } catch (e: Exception) {
        "Rp - "
    }
}

fun getDays(checkIn: Date, checkOut: Date):Int{
    val millionSeconds = checkOut.time - checkIn.time
    return TimeUnit.MILLISECONDS.toDays(millionSeconds).toInt()
}
