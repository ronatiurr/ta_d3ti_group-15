package com.app.common.preference;

import android.content.Context;
import android.content.SharedPreferences;

class LocalPreferences(mContext: Context, name: String) {

    private val sharedPreference: SharedPreferences = mContext.getSharedPreferences(name, Context.MODE_PRIVATE)

    var token: String?
        get() = sharedPreference.getString(PrefKey.TOKEN, null)
        set(token) = sharedPreference.edit().putString(PrefKey.TOKEN, token).apply()

    var username: String?
        get() = sharedPreference.getString(PrefKey.USERNAME, null)
        set(username) = sharedPreference.edit().putString(PrefKey.USERNAME, username).apply()

    var id: Int
        get() = sharedPreference.getInt(PrefKey.ID, 0)
        set(id) = sharedPreference.edit().putInt(PrefKey.ID, id).apply()

    var isLogin: Boolean
        get() = sharedPreference.getBoolean(PrefKey.ISLOGIN, false)
        set(isLogin) = sharedPreference.edit().putBoolean(PrefKey.ISLOGIN, isLogin).apply()

    companion object {
        private val TAG = "LocalPreferences"
    }

}

