package com.app.visitor.activity.homestay.detail

import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class HomestayDetailModule {
    @Provides
    internal fun provideHomestayDetailView(activity: HomestayDetailActivity): HomestayDetailContract.View {
        return activity
    }

    @Provides
    internal fun provideHomestayDetailPresenter(view: HomestayDetailContract.View, networkSDK: NetworkSDK): HomestayDetailPresenter {
        return HomestayDetailPresenter(view, networkSDK)
    }
}
