package com.app.visitor.activity.login

import com.app.common.preference.LocalPreferences
import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class LoginModule {

    @Provides
    internal fun provideLoginView(activity: LoginActivity): LoginContract.View {
        return activity
    }

    @Provides
    internal fun provideLoginPresenter(view: LoginContract.View, networkSDK: NetworkSDK, localPreferences: LocalPreferences): LoginPresenter {
        return LoginPresenter(view, networkSDK, localPreferences)
    }

}
