package com.app.visitor.activity.main

import com.app.network.model.response.SearchHomestayResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class MainPresenter(private val view: MainContract.View, private val networkSDK: NetworkSDK) : MainContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun search(request: String) {
        view.loading()

        networkSDK.getHomestayByKecamatan(request, compositeDisposable, object : ResponseCallback<SearchHomestayResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: SearchHomestayResponse) {
                if (response.isSuccess()) view.successSearch(response)
                else view.failedSearch(response.message)

                view.dismissLoading()
            }

        })
    }
}
