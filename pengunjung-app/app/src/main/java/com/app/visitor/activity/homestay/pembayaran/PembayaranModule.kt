package com.app.visitor.activity.homestay.pembayaran

import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class PembayaranModule {
    @Provides
    internal fun providePembayaranView(activity: PembayaranActivity): PembayaranContract.View {
        return activity
    }

    @Provides
    internal fun providePembayaranPresenter(view: PembayaranContract.View, networkSDK: NetworkSDK): PembayaranPresenter {
        return PembayaranPresenter(view, networkSDK)
    }
}