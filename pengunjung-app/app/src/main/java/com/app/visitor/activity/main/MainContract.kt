package com.app.visitor.activity.main

import com.app.network.model.response.SearchHomestayResponse

class MainContract {

    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successSearch(response: SearchHomestayResponse)
        fun failedSearch(message: String)
    }

    interface Presenter {
        fun search(request: String)
    }
}
