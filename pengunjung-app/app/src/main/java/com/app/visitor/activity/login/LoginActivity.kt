package com.app.visitor.activity.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.app.network.model.request.LoginRequest
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import com.app.visitor.activity.main.MainActivity
import com.app.visitor.activity.register.RegisterActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View {

    @Inject lateinit var presenter: LoginPresenter

    companion object {
        private val TAG = "LoginActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initListener()
    }

    private fun initListener(){
        username.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isValidBtn()
            }
        })

        password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isValidBtn()
            }
        })

        regBtn.setOnClickListener{ startActivity(RegisterActivity.newIntent(this)) }
        loginBtn.setOnClickListener{ presenter.login(LoginRequest(username.text.toString(), password.text.toString())) }
    }

    private fun isValidBtn(){
        if (username.text.isNotEmpty() && password.text.isNotEmpty()){
            loginBtn.isEnabled = true
            loginBtn.setBackgroundResource(R.drawable.bg_btn_enable)
        } else {
            loginBtn.isEnabled = false
            loginBtn.setBackgroundResource(R.drawable.bg_btn_disabled)
        }
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successLogin() {
        startActivity(MainActivity.newIntent(this))
        finish()
    }

    override fun failedLogin(message: String) {
        toast(message)
    }
}
