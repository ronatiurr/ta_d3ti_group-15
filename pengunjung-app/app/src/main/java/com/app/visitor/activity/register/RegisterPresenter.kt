package com.app.visitor.activity.register

import com.app.network.model.request.RegisterRequest
import com.app.network.model.response.BaseResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class RegisterPresenter (private val view: RegisterContract.View,
                         private val networkSDK: NetworkSDK) : RegisterContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun register(request: RegisterRequest) {
        view.loading()

        networkSDK.register(request, compositeDisposable, object : ResponseCallback<BaseResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successRegister()
                else view.failedRegister(response.message)

                view.dismissLoading()
            }

        })
    }
}
