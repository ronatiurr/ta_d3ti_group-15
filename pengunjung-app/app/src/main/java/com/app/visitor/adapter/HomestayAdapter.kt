package com.app.visitor.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.network.model.response.HomestayDetail
import com.app.uicustom.adapter.BaseHolder
import com.app.visitor.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_homestay.view.*
import java.lang.Exception

class HomestayAdapter(private val listener: HomestayAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = ArrayList<HomestayDetail>()

    inner class ViewHolder(parent: ViewGroup) : BaseHolder(R.layout.adapter_homestay, parent){

        fun bind(data: HomestayDetail){
            if (data.gambar.isNotEmpty()) {
                Picasso.get()
                        .load(data.gambar)
                        .fit()
                        .into(itemView.img, object : Callback{
                            override fun onSuccess() {
                                itemView.lihat.visibility = View.VISIBLE
                            }

                            override fun onError(e: Exception?) {}
                        })
            } else itemView.lihat.visibility = View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = ViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[holder.adapterPosition]
        val viewHolder = holder as ViewHolder

        viewHolder.bind(item)
        viewHolder.itemView.lihat.setOnClickListener{
            listener.onClickItemAdapter(item.id)
        }
    }

    fun initData(items: ArrayList<HomestayDetail>){
        this.items = items
        notifyDataSetChanged()
    }

    interface HomestayAdapterListener{
        fun onClickItemAdapter(id: String)
    }
}
