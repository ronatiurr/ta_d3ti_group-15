package com.app.visitor.activity.homestay.detail

import com.app.network.model.response.GetHomestayResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import com.app.visitor.activity.homestay.detail.HomestayDetailContract
import io.reactivex.disposables.CompositeDisposable

class HomestayDetailPresenter (private val view: HomestayDetailContract.View,
                               private val networkSDK: NetworkSDK) : HomestayDetailContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getData(request: String) {
        view.loading()

        networkSDK.getHomestay(request, compositeDisposable, object : ResponseCallback<GetHomestayResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: GetHomestayResponse) {
                if (response.isSuccess()) view.successGetData(response.data)
                else view.failedGetData(response.message)

                view.dismissLoading()
            }

        })
    }
}
