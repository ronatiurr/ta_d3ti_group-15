package com.app.visitor.activity.homestay.pemesanan.detail

import com.app.common.preference.LocalPreferences
import com.app.network.model.response.GetPemesananResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class DetailPesananPresenter (private val view: DetailPesananContract.View,
                              private val networkSDK: NetworkSDK,
                              private val localPreferences: LocalPreferences) : DetailPesananContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getDetail() {
        view.loading()

        networkSDK.getPemesanan(localPreferences.id.toString(), compositeDisposable, object : ResponseCallback<GetPemesananResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: GetPemesananResponse) {
                if (response.isSuccess()) view.successGetDetail(response.data!!)
                else view.failedGetDetail(response.message)

                view.dismissLoading()
            }

        })
    }
}
