package com.app.visitor.activity.homestay.pemesanan.detail

import com.app.network.model.response.DataPemesanan
import com.app.network.model.response.GetPemesananResponse

class DetailPesananContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successGetDetail(response: DataPemesanan)
        fun failedGetDetail(message: String)
    }

    interface Presenter {
        fun getDetail()
    }
}
