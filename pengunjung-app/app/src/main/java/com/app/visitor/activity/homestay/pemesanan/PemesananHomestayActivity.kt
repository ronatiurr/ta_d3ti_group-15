package com.app.visitor.activity.homestay.pemesanan

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.app.common.preference.LocalPreferences
import com.app.common.utils.IntentKey
import com.app.common.utils.convertToRp
import com.app.common.utils.getDays
import com.app.common.utils.stringToDate
import com.app.network.model.request.PemesananRequest
import com.app.network.model.response.DataKamar
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import com.app.visitor.activity.homestay.pemesanan.detail.DetailPesananActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_pemesanan.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class PemesananHomestayActivity : BaseActivity(), PemesananHomestayContract.View {

    @Inject lateinit var presenter: PemesananHomestayPresenter
    @Inject lateinit var localPreferences: LocalPreferences
    private var price : Int = 0
    private var id = ""
    private var request = PemesananRequest()

    companion object {
        private val TAG = "PemesananHomestayActivity"

        @JvmStatic fun newIntent(context: Context, id: String, fasilitas: String): Intent {
            val i = Intent(context, PemesananHomestayActivity::class.java)
            i.putExtra(IntentKey.KAMARID, id)
            i.putExtra(IntentKey.FASILITAS, fasilitas)

            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pemesanan)

        getData()
        initListener()
    }

    private fun getData(){
        intent.extras?.let { intent -> (intent.getString(IntentKey.KAMARID)).let {
            presenter.getKamar(it!!)
        } }

        intent.extras?.let { intent -> (intent.getString(IntentKey.FASILITAS)).let {
            fasilitas.text = it
        } }
    }

    private fun setSpinner(jumlah: Int){
        val roomList = ArrayList<String>()
        val tamuList = ArrayList<String>()

        for (i in 1..jumlah+1) {
            roomList.add(i.toString())
        }

        for (i in 1..11) {
            tamuList.add(i.toString())
        }

        val roomAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, roomList)
        roomAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        room.adapter = roomAdapter

        val tamuAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, tamuList)
        tamuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        tamu.adapter = tamuAdapter

        room.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                request.jumlahKamar = roomAdapter.getItem(position)!!.toInt()

                getPrice()
                isValidBtn()
            }
        }

        tamu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                request.jumlahTamu = tamuAdapter.getItem(position)!!.toInt()

                getPrice()
                isValidBtn()
            }
        }
    }

    private fun initListener(){
        checkIn.setOnClickListener {
            val newFragment = DatePickerFragment()

            newFragment.setDatePickerFragmentListener(object : DatePickerFragment.DatePickerFragmentListener {
                override fun getDateSet(date: String) {
                    request.checkIn = date
                    request.checkOut = ""
                    checkIn.text = date
                    checkOut.setText(R.string.check2)

                    getPrice()
                    isValidBtn()
                }

            })
            newFragment.show(supportFragmentManager, "datePicker")
        }

        checkOut.setOnClickListener {
            val newFragment = DatePickerFragment()

            newFragment.setDatePickerFragmentListener(object : DatePickerFragment.DatePickerFragmentListener {
                override fun getDateSet(date: String) {
                    request.checkOut = date
                    checkOut.text = date

                    getPrice()
                    isValidBtn()
                }

            })
            newFragment.show(supportFragmentManager, "datePicker")
        }

        next.setOnClickListener {
            presenter.pesanKamar(request)
        }
    }

    private fun getPrice(){
        if (request.checkIn.isNotEmpty()
                && request.checkOut.isNotEmpty()
                && (request.jumlahKamar != 0)) {
            val days = getDays(stringToDate(request.checkIn), stringToDate(request.checkOut))

            request.total = price*days*request.jumlahKamar
            harga.text = convertToRp(request.total.toLong())
        } else {
            request.total = 0
            harga.text = convertToRp(0)
        }
    }

    private fun isValidBtn(){
        if (request.isValid()){
            next.isEnabled = true
            next.setBackgroundResource(R.drawable.bg_btn_enable)
        } else {
            next.isEnabled = false
            next.setBackgroundResource(R.drawable.bg_btn_disabled)
        }
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successGetKamar(response: DataKamar) {
        request.kamar = response.id
        request.user = localPreferences.id.toString()
        request.homestay = response.homestay
        price = response.harga.toInt()

        setSpinner(response.jumlah)
    }

    override fun failedGetKamar(message: String) {
        toast(message)
    }

    class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

        private var listener: DatePickerFragmentListener? =null
        private val dateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH)

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dialog = DatePickerDialog(requireContext(), this, year, month, day)
            dialog.datePicker.minDate = c.timeInMillis
            return dialog
        }

        override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, day)
            listener?.getDateSet(dateFormat.format(calendar.timeInMillis))
        }

        fun setDatePickerFragmentListener(listener: DatePickerFragmentListener) {
            this.listener = listener
        }
        interface DatePickerFragmentListener {
            fun getDateSet(date: String)
        }
    }

    override fun successPesanKamar() {
        startActivity(DetailPesananActivity.newIntent(this))
    }

    override fun failedPesanKamar(message: String) {
        toast(message)
    }

}