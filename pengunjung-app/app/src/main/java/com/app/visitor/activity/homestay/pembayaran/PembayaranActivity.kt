package com.app.visitor.activity.homestay.pembayaran

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.common.utils.IntentKey
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_pembayaran.*
import kotlinx.android.synthetic.main.activity_pemesanan.*
import javax.inject.Inject

class PembayaranActivity : BaseActivity(), PembayaranContract.View {

    @Inject lateinit var presenter: PembayaranPresenter

    companion object {
        private val TAG = "PembayaranActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, PembayaranActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pembayaran)

        dp.setOnClickListener {
            startActivity(Intent(this, TransferDPActivity::class.java))
        }

        cod.setOnClickListener {
            startActivity(Intent(this, BayarDiTempatActivity::class.java))
        }

        lunas.setOnClickListener {
            startActivity(Intent(this, TransferLunasActivity::class.java))
        }
    }

    private fun initListener(){

    }

}