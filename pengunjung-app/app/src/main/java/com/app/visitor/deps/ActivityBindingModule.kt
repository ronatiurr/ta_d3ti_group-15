package com.app.visitor.deps

import com.app.visitor.activity.SplashActivity
import com.app.visitor.activity.homestay.detail.HomestayDetailActivity
import com.app.visitor.activity.homestay.detail.HomestayDetailModule
import com.app.visitor.activity.homestay.pembayaran.PembayaranActivity
import com.app.visitor.activity.homestay.pembayaran.PembayaranModule
import com.app.visitor.activity.homestay.pemesanan.PemesananHomestayActivity
import com.app.visitor.activity.homestay.pemesanan.PemesananHomestayModule
import com.app.visitor.activity.homestay.pemesanan.detail.DetailPesananActivity
import com.app.visitor.activity.homestay.pemesanan.detail.DetailPesananModule
import com.app.visitor.activity.login.LoginActivity
import com.app.visitor.activity.login.LoginModule
import com.app.visitor.activity.main.MainActivity
import com.app.visitor.activity.main.MainModule
import com.app.visitor.activity.register.RegisterActivity
import com.app.visitor.activity.register.RegisterModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector
    internal abstract fun splashActivity(): SplashActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LoginModule::class])
    internal abstract fun loginActivity(): LoginActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [RegisterModule::class])
    internal abstract fun registerActivity(): RegisterActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [HomestayDetailModule::class])
    internal abstract fun homestayDetailActivity(): HomestayDetailActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [PemesananHomestayModule::class])
    internal abstract fun pemesananHomestayActivity(): PemesananHomestayActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [DetailPesananModule::class])
    internal abstract fun detailPesananActivity(): DetailPesananActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [PembayaranModule::class])
    internal abstract fun pembayaranActivity(): PembayaranActivity
}
