package com.app.visitor.activity.homestay.pemesanan

import com.app.network.model.request.PemesananRequest
import com.app.network.model.response.BaseResponse
import com.app.network.model.response.GetKamarResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class PemesananHomestayPresenter (private val view: PemesananHomestayContract.View,
                                  private val networkSDK: NetworkSDK) : PemesananHomestayContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getKamar(id: String) {
        view.loading()

        networkSDK.getKamar(id, compositeDisposable, object : ResponseCallback<GetKamarResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: GetKamarResponse) {
                if (response.isSuccess()) view.successGetKamar(response.data!!)
                else view.failedGetKamar(response.message)

                view.dismissLoading()
            }

        })
    }

    override fun pesanKamar(request: PemesananRequest) {
        view.loading()

        networkSDK.pesanKamar(request, compositeDisposable, object : ResponseCallback<BaseResponse>{
            override fun onError(throwable: Throwable) {
                view.dismissLoading()
                view.failedNetwork()
            }

            override fun onSuccess(response: BaseResponse) {
                if (response.isSuccess()) view.successPesanKamar()
                else view.failedPesanKamar(response.message)

                view.dismissLoading()
            }

        })
    }
}