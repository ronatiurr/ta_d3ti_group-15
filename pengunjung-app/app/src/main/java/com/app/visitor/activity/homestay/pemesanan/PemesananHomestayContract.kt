package com.app.visitor.activity.homestay.pemesanan

import com.app.network.model.request.PemesananRequest
import com.app.network.model.response.DataKamar

class PemesananHomestayContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successGetKamar(response: DataKamar)
        fun failedGetKamar(message: String)
        fun successPesanKamar()
        fun failedPesanKamar(message: String)
    }

    interface Presenter {
        fun getKamar(id: String)
        fun pesanKamar(request: PemesananRequest)
    }
}