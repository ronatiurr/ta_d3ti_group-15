package com.app.visitor.activity.homestay.pemesanan

import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class PemesananHomestayModule {
    @Provides
    internal fun providePemesananView(activity: PemesananHomestayActivity): PemesananHomestayContract.View {
        return activity
    }

    @Provides
    internal fun providePemesananPresenter(view: PemesananHomestayContract.View, networkSDK: NetworkSDK): PemesananHomestayPresenter {
        return PemesananHomestayPresenter(view, networkSDK)
    }
}