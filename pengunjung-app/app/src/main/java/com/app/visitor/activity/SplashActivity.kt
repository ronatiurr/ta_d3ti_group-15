package com.app.visitor.activity

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.common.preference.LocalPreferences
import com.app.visitor.R
import com.app.visitor.activity.login.LoginActivity
import com.app.visitor.activity.main.MainActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject lateinit var localPreferences: LocalPreferences
    private val mWaitHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        checkInData()
    }

    private fun checkInData() {
        mWaitHandler.postDelayed({
            try {
                if (localPreferences.isLogin) startActivity(MainActivity.newIntent(this))
                else startActivity(LoginActivity.newIntent(this))

                finish()
            } catch (ignored: Exception) {
                ignored.printStackTrace()
            }
        }, 2000)
    }
}
