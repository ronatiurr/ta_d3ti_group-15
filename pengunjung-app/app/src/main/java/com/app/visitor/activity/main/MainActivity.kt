package com.app.visitor.activity.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.network.model.response.SearchHomestayResponse
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import com.app.visitor.activity.homestay.detail.HomestayDetailActivity
import com.app.visitor.adapter.HomestayAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, HomestayAdapter.HomestayAdapterListener{

    @Inject lateinit var presenter: MainPresenter
    private var request = -1
    private var adapter = HomestayAdapter(this)

    companion object {
        private val TAG = "MainActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureToolbar("", true)

        initAdapter()
        initSpinner()
        initListener()
    }

    private fun initAdapter() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcList.layoutManager = layoutManager
        rcList.adapter = adapter
    }

    private fun initSpinner(){
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOf("Ajibata", "Balige", "Bonatua Lunasi", "Borbor", "Habinsaran", "Laguboti",
                "Lumban Julu", "Nassau", "Parmaksian", "Pintu Pohan", "Porsea", "Siantar Narumonda", "Sigumpar", "Silaen", "Tampahan", "Uluan"))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                request = (adapter.getItemId(position).toInt()) + 1
                kecamatanTitle.text = "Kecamatan ${adapter.getItem(position)}"
                isValidBtn()
            }
        }
    }

    private fun initListener(){
        button.setOnClickListener { presenter.search(request.toString()) }
    }

    private fun isValidBtn(){
        button.isEnabled = request != -1
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successSearch(response: SearchHomestayResponse) {
        searchLyt.visibility = View.GONE
        listLyt.visibility = View.VISIBLE
        titleTxt.visibility = View.GONE

        adapter.initData(response.data)
    }

    override fun failedSearch(message: String) {
        toast(message)
    }

    override fun onClickItemAdapter(id: String) {
        startActivity(HomestayDetailActivity.newIntent(this, id))
    }
}
