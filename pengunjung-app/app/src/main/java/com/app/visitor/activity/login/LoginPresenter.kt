package com.app.visitor.activity.login

import com.app.common.preference.LocalPreferences
import com.app.network.model.request.LoginRequest
import com.app.network.model.response.LoginResponse
import com.app.network.response.ResponseCallback
import com.app.network.sdk.NetworkSDK
import io.reactivex.disposables.CompositeDisposable

class LoginPresenter(private val view: LoginContract.View,
                     private val networkSDK: NetworkSDK,
                     private val localPreferences: LocalPreferences) : LoginContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun login(request: LoginRequest) {
        view.loading()
        networkSDK.login(request, compositeDisposable, object : ResponseCallback<LoginResponse> {

            override fun onError(throwable: Throwable) {
                view.failedNetwork()
                view.dismissLoading()
            }

            override fun onSuccess(response: LoginResponse) {
                if (response.isSuccess()) {
                    localPreferences.isLogin = true
                    localPreferences.username = response.data!!.username
                    localPreferences.id = response.data!!.id.toInt()
                    view.successLogin()
                } else view.failedLogin(response.message)

                view.dismissLoading()
            }
        })
    }
}
