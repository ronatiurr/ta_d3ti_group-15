package com.app.visitor.activity.homestay.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.app.common.utils.IntentKey
import com.app.network.model.response.HomestayDetail
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import com.app.visitor.activity.homestay.pemesanan.PemesananHomestayActivity
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail_homestay.*
import javax.inject.Inject

class HomestayDetailActivity : BaseActivity(), HomestayDetailContract.View {

    @Inject lateinit var presenter: HomestayDetailPresenter
    private var oneBedRoom = ""
    private var twoBedRoom = ""
    private var fasilitas = ""

    companion object {
        private val TAG = "LoginActivity"

        @JvmStatic fun newIntent(context: Context, id: String): Intent {
            val i = Intent(context, HomestayDetailActivity::class.java)
            i.putExtra(IntentKey.HOMESTAYID, id)

            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_homestay)

        getData()
        initListener()
    }

    private fun initListener(){
        pesanOneBed.setOnClickListener {
            toPemesanan(oneBedRoom)
        }

        pesanTwoBed.setOnClickListener {
            toPemesanan(twoBedRoom)
        }
    }

    private fun getData(){
        intent.extras?.let { intent -> (intent.getSerializable(IntentKey.HOMESTAYID) as String).let {
            presenter.getData(it)
        } }
    }

    private fun toPemesanan(room: String){
        startActivity(PemesananHomestayActivity.newIntent(this, room, fasilitas))
        finish()
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successGetData(response: HomestayDetail) {
        response.let {
            oneBedRoom = it.oneBedRoom.toString()
            twoBedRoom = it.twoBedRoom.toString()
            fasilitas = it.poi
            nama.text = it.nama
            keterangan.text = it.keterangan
            poi.text = it.poi
            alamat.text = it.alamat
            oneBed.setText("${it.oneBed} bed")
            twoBed.setText("${it.twoBed} bed")

            if (it.gambar.isNotEmpty()){
                Picasso.get().load(it.gambar).fit().into(homestay)
            }

            if (it.oneBed > 0) {
                pesanOneBed.isEnabled = true
                pesanOneBed.setBackgroundResource(R.drawable.bg_btn_enable)
            } else {
                pesanOneBed.isEnabled = false
                pesanOneBed.setBackgroundResource(R.drawable.bg_btn_disabled)
            }

            if (it.twoBed > 0) {
                pesanTwoBed.isEnabled = true
                pesanTwoBed.setBackgroundResource(R.drawable.bg_btn_enable)
            } else {
                pesanTwoBed.isEnabled = false
                pesanTwoBed.setBackgroundResource(R.drawable.bg_btn_disabled)
            }

            /**
             * add maps feature
             * */
            lokasi.setOnClickListener {
                startActivity(Intent(this, MapsActivity::class.java))
            }
        }
    }

    override fun failedGetData(message: String) {
        toast(message)
    }
}
