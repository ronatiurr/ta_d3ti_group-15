package com.app.visitor.activity.homestay.pemesanan.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.common.utils.convertToRp
import com.app.common.utils.getDays
import com.app.common.utils.stringToDate
import com.app.network.model.response.DataPemesanan
import com.app.visitor.R
import com.app.visitor.activity.base.BaseActivity
import com.app.visitor.activity.homestay.pembayaran.PembayaranActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_rincian_akhir.*
import javax.inject.Inject

class DetailPesananActivity : BaseActivity(), DetailPesananContract.View {

    @Inject lateinit var presenter: DetailPesananPresenter

    companion object {
        private val TAG = "DetailPesananActivity"

        @JvmStatic fun newIntent(context: Context): Intent {
            return Intent(context, DetailPesananActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rincian_akhir)

        initListener()
        presenter.getDetail()
    }

    private fun initListener(){
        next.setOnClickListener {
            startActivity(PembayaranActivity.newIntent(this))
        }
    }

    override fun loading() {
        showLoading()
    }

    override fun dismissLoading() {
        cancelLoading()
    }

    override fun failedNetwork() {
        toast(R.string.something_went_wrong)
    }

    override fun successGetDetail(response: DataPemesanan) {
        response.let {
            days.text = "${getDays(stringToDate(it.checkIn), stringToDate(it.checkOut))} Malam"
            checkIn.text = it.checkIn
            checkOut.text = it.checkOut
            room.text = "Kamar : ${it.jumlahKamar}"
            tamu.text = "Tamu : ${it.jumlahTamu}"
            harga.text = "Harga: ${convertToRp(it.total.toLong())}"
        }
    }

    override fun failedGetDetail(message: String) {
        toast(message)
    }

}
