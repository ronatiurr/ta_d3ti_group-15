package com.app.visitor.activity.homestay.detail

import com.app.network.model.response.HomestayDetail

class HomestayDetailContract {
    interface View {
        fun loading()
        fun dismissLoading()
        fun failedNetwork()
        fun successGetData(response: HomestayDetail)
        fun failedGetData(message: String)
    }

    interface Presenter{
        fun getData(request: String)
    }
}
