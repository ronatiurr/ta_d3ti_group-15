package com.app.visitor.activity.homestay.pemesanan.detail

import com.app.common.preference.LocalPreferences
import com.app.network.sdk.NetworkSDK
import dagger.Module
import dagger.Provides

@Module
class DetailPesananModule {
    @Provides
    internal fun provideDetailPesananView(activity: DetailPesananActivity): DetailPesananContract.View {
        return activity
    }

    @Provides
    internal fun provideDetailPesananPresenter(view: DetailPesananContract.View, networkSDK: NetworkSDK, localPreferences: LocalPreferences): DetailPesananPresenter {
        return DetailPesananPresenter(view, networkSDK, localPreferences)
    }
}
